import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'tt_gala_darbs_python.settings')

application = get_wsgi_application()
