from django.urls import path
from . import views

urlpatterns = [
    path("api-test", views.api_test, name='api-test'),
    path("login", views.login, name='login'),
    path("retrieve-community-posts", views.retrieve_community_posts, name='retrieve-community-posts'),
    path("create-community-post", views.create_community_post, name='create-community-post'),
    path("retrieve-post-content", views.retrieve_post_content, name='retrieve-post-content'),
    path("retrieve-post-comments", views.retrieve_post_comments, name='retrieve-post-comments'),
    path("post-comment", views.post_comment, name='post-comment'),
    path("get-all-communities", views.get_all_communities, name='get-all-communities'),
    path("get-community-description", views.get_community_description, name='get-community-description'),
    path("like-post", views.like_post, name='like-post'),
    path("like-comment", views.like_comment, name='like-comment'),
    path("retrieve-home-posts", views.retrieve_home_posts, name='retrieve-home-posts'),
    path("post-is-liked", views.post_is_liked, name='post-is-liked'),
    path("subscribe-to-community", views.subscribe_to_community, name='subscribe-to-community'),
    path("is-subscribed", views.is_subscribed, name='is-subscribed'),
    path("change-password", views.change_password, name='change-password'),
    path("register", views.register, name='register'),
    path("delete-post", views.delete_post, name='delete-post'),
    path("is-admin", views.is_admin, name='is-admin')
]