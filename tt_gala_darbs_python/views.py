import random

from django.http import HttpResponse
import json
from datetime import datetime
from . import databaseutils
import re



# Tests that the API is capable of returning a response
def api_test(request):
    r = dict()
    r['status'] = 'OK'
    r['server-time'] = datetime.now().strftime('%d/%m/%Y %H:%M:%S')
    return HttpResponse(json.dumps(r))


# Parses a login request
def login(request):
    r = dict()

    # retrieves the request arguments
    args = json.loads(request.body.decode('utf-8'))
    # checks if the request has the appropriate parameters
    if 'username' not in args or 'password' not in args:
        r['status'] = 'FAILED'
        return HttpResponse(json.dumps(r))

    # retrieves the parameters
    username = args['username']
    password = args['password']

    if username is None or password is None:
        r['status'] = 'FAILED'
        return HttpResponse(json.dumps(r))

    login_correct = databaseutils.check_login(username, password)
    if not login_correct:
        r['status'] = 'FAILED'
        return HttpResponse(json.dumps(r))
    else:
        # retrieves the user ID corresponding to the username
        user_id = databaseutils.get_id_from_username(username.lower())
        # creates a session for the specific user
        session = databaseutils.create_session(user_id)

        r['status'] = 'OK'
        r['csrf-token'] = session[1]

        response = HttpResponse(json.dumps(r))
        response.set_cookie('token', session[0], httponly=True)
        return response


#
def retrieve_community_posts(request):
    r = dict()

    # retrieves the request arguments
    args = json.loads(request.body.decode('utf-8'))

    if 'community-name' not in args:
        r['status'] = 'FAILED'
        return HttpResponse(json.dumps(r))

    community_name = args['community-name']
    community_id = databaseutils.get_community_id(community_name)

    if community_id is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-community'
        return HttpResponse(json.dumps(r))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    user_id = -1
    if session_info is not None:
        user_id = session_info[0][1]

    posts = databaseutils.get_community_posts(community_id, user_id, 10)

    r['status'] = 'OK'
    r['posts'] = posts

    return HttpResponse(json.dumps(r))


def retrieve_home_posts(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    user_id = -1
    if session_info is not None:
        user_id = session_info[0][1]

    posts = databaseutils.get_home_posts(user_id)

    r['status'] = 'OK'
    r['posts'] = posts

    return HttpResponse(json.dumps(r))


def post_is_liked(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if 'post-id' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    if databaseutils.get_post_content(args['post-id']) is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-post'
        return HttpResponse(json.dumps(r))

    r['liked'] = databaseutils.post_is_liked(args['post-id'], session_info[0][1])
    r['status'] = 'OK'

    return HttpResponse(json.dumps(r))


def create_community_post(request):
    r = dict()

    # retrieves the request arguments
    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    post_title = args.get("post-title")
    post_content = args.get("post-content")
    post_community = args.get("post-community")

    community_id = databaseutils.get_community_id(post_community)

    if community_id is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-community'
        return HttpResponse(json.dumps(r))

    if post_title is None or post_content is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    if post_title.strip() == '' or post_content.strip() == '':
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    post_id = str(random.randrange(1, 999999999999999999))

    try:
        databaseutils.create_community_post(post_id, post_content, session_info[0][1], post_title, community_id)
    except:
        r['status'] = 'FAILED'
        r['status-code'] = 'internal-error'
        return HttpResponse(json.dumps(r))

    r['status'] = 'OK'
    r['post-id'] = post_id
    return HttpResponse(json.dumps(r))


def retrieve_post_content(request):
    r = dict()

    # retrieves the request arguments
    args = json.loads(request.body.decode('utf-8'))

    if 'post-id' not in args:
        r['status'] = 'FAILED'
        return HttpResponse(json.dumps(r))

    post_id = args['post-id']
    post = databaseutils.get_post_content(post_id)

    if post is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-post'
        return HttpResponse(json.dumps(r))

    r['status'] = 'OK'

    r['author'] = post[1]
    r['community'] = post[2]
    r['post-content'] = post[3]
    r['timestamp'] = post[4]
    r['title'] = post[5]

    return HttpResponse(json.dumps(r))


def retrieve_post_comments(request):
    r = dict()

    # retrieves the request arguments
    args = json.loads(request.body.decode('utf-8'))

    comments = databaseutils.get_post_comments(args['post-id'])

    return HttpResponse(json.dumps(comments))


def post_comment(request):
    r = dict()

    # retrieves the request arguments
    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    content = args['content']
    parent_post = args['parent-post']
    parent_comment = None

    if 'parent-comment' in args:
        parent_comment = args['parent-comment']

    if content is None or parent_post is None or content.strip() == '':
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    if databaseutils.get_post_content(parent_post) is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-parent-post'
        return HttpResponse(json.dumps(r))

    if parent_comment is not None and not databaseutils.check_comment_exists(parent_comment):
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-parent-comment'
        return HttpResponse(json.dumps(r))

    comment_id = str(random.randrange(1, 999999999999999999))

    databaseutils.add_comment(comment_id, content, session_info[0][1], parent_post, parent_comment)

    r['status'] = 'OK'

    return HttpResponse(json.dumps(r))


def get_all_communities(request):
    r = list()

    all_communities = databaseutils.get_all_communities()
    for row in all_communities:
        r.append(row[0])

    r.sort()
    return HttpResponse(json.dumps(r))


def get_community_description(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    if 'community' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-community'
        return HttpResponse(json.dumps(r))

    community_id = databaseutils.get_community_id(args['community'])

    if community_id is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-community'
        return HttpResponse(json.dumps(r))

    r['status'] = 'OK'
    description = databaseutils.get_community_description(community_id)

    if description is not None:
        description = description[0][0]

    r['description'] = description

    return HttpResponse(json.dumps(r))


def like_post(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    if 'post-id' not in args or 'remove-like' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    post_id = args['post-id']
    remove_like = str(args['remove-like']).lower()

    if remove_like not in ['true', 'false']:
        r['status'] = 'FAILED'
        r['status-code'] = 'invalid-parameters'
        return HttpResponse(json.dumps(r))

    if 't' in remove_like or 'T' in remove_like or remove_like is True:
        remove_like = True
    else:
        remove_like = False

    if databaseutils.get_post_content(post_id) is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-post'
        return HttpResponse(json.dumps(r))

    if databaseutils.like_post(session_info[0][1], post_id, remove_like):
        r['status'] = 'OK'
        return HttpResponse(json.dumps(r))
    else:
        r['status'] = 'FAILED'
        r['status-code'] = 'internal-error'
        return HttpResponse(json.dumps(r))


def is_admin(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    r['status'] = 'OK'
    if session_info[0][6] == 2:
        r['is-admin'] = True
    else:
        r['is-admin'] = False

    return HttpResponse(json.dumps(r))


def delete_post(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    if 'post-id' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    post_id = args['post-id']
    post = databaseutils.get_post_content(post_id)

    if post is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-post'
        return HttpResponse(json.dumps(r))

    if post[0] == session_info[0][1] or session_info[0][6] == 2:
        databaseutils.delete_post(post_id)
        r['status'] = 'OK'
        return HttpResponse(json.dumps(r))
    else:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-authenticated'
        return HttpResponse(json.dumps(r))


def register(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    if 'username' not in args or 'password' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    username = args['username'].lower()
    password = args['password']

    if databaseutils.get_id_from_username(username) is not None:
        r['status'] = 'FAILED'
        r['status-code'] = 'account-already-exists'
        return HttpResponse(json.dumps(r))

    if len(password) < 8:
        r['status'] = 'FAILED'
        r['status-code'] = 'password-too-short'
        return HttpResponse(json.dumps(r))

    if not re.search('^[a-z]([a-z0-9_]+)$', username):
        r['status'] = 'FAILED'
        r['status-code'] = 'invalid-username'
        return HttpResponse(json.dumps(r))

    if len(username) < 6 or len(username) > 18:
        r['status'] = 'FAILED'
        r['status-code'] = 'invalid-username'
        return HttpResponse(json.dumps(r))

    databaseutils.create_account(username, password)

    r['status'] = 'OK'
    return HttpResponse(json.dumps(r))


def change_password(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    if 'current-password' not in args or 'new-password' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    if databaseutils.check_login(session_info[0][2], args['current-password']):
        if len(args['new-password']) < 8:
            r['status'] = 'FAILED'
            r['status-code'] = 'password-too-short'
            return HttpResponse(json.dumps(r))
        else:
            databaseutils.change_password(session_info[0][1], args['new-password'])
            r['status'] = 'OK'
            return HttpResponse(json.dumps(r))

    r['status'] = 'FAILED'
    r['status-code'] = 'incorrect-login'
    return HttpResponse(json.dumps(r))


def is_subscribed(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    if 'community-name' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    community_id = databaseutils.get_community_id(args['community-name'])

    if community_id is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-community'
        return HttpResponse(json.dumps(r))

    r['is-subscribed'] = databaseutils.user_is_subscribed(session_info[0][1], community_id)
    return HttpResponse(json.dumps(r))


def subscribe_to_community(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    if 'community-name' not in args or 'remove-subscription' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    community_name = args['community-name']

    community_id = databaseutils.get_community_id(community_name)
    if community_id is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-community'
        return HttpResponse(json.dumps(r))

    remove_subscription = str(args['remove-subscription']).lower()

    if 't' in remove_subscription or 'T' in remove_subscription or remove_subscription is True:
        remove_subscription = True
    else:
        remove_subscription = False

    if databaseutils.subscribe_to_community(session_info[0][1], community_id, remove_subscription):
        r['status'] = 'OK'
    else:
        r['status'] = 'FAILED'
        r['status-code'] = 'internal-error'

    return HttpResponse(json.dumps(r))


def like_comment(request):
    r = dict()

    args = json.loads(request.body.decode('utf-8'))

    session_token = request.COOKIES.get("token")
    csrf_token = args.get("csrf-token")

    if session_token is None or csrf_token is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    session_info = databaseutils.get_session_info(session_token, csrf_token)

    if session_info is None:
        r['status'] = 'FAILED'
        r['status-code'] = 'not-logged-in'
        return HttpResponse(json.dumps(r))

    if 'comment-id' not in args or 'remove-like' not in args:
        r['status'] = 'FAILED'
        r['status-code'] = 'missing-parameters'
        return HttpResponse(json.dumps(r))

    comment_id = args['comment-id']
    remove_like = str(args['remove-like']).lower()

    if remove_like not in ['true', 'false']:
        r['status'] = 'FAILED'
        r['status-code'] = 'invalid-parameters'
        return HttpResponse(json.dumps(r))

    if remove_like is 'true':
        remove_like = True
    else:
        remove_like = False

    if not databaseutils.check_comment_exists(comment_id):
        r['status'] = 'FAILED'
        r['status-code'] = 'no-such-comment'
        return HttpResponse(json.dumps(r))

    if databaseutils.like_comment(session_info[0][1], comment_id, remove_like):
        r['status'] = 'OK'
        return HttpResponse(json.dumps(r))
    else:
        r['status'] = 'FAILED'
        r['status-code'] = 'internal-error'
        return HttpResponse(json.dumps(r))

