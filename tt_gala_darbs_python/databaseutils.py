import threading
from datetime import datetime

from passlib.hash import pbkdf2_sha256
import pyodbc
import random
import secrets

# defines the database access credentials
uid = 'tt_python'
pwd = 'tt-python'
# creates a database connection
conn = pyodbc.connect(
    'DRIVER={ODBC Driver 17 for SQL Server};SERVER=localhost;DATABASE=TT_DB;Trusted_Connection=no;Uid=' + uid +
    ';pwd=' + pwd)

lock = threading.Lock()


def execute_query(query, params, return_result):
    results = None

    with lock:
        cursor = conn.cursor()

        # executes the query
        cursor.execute(query, params)

        if return_result:
            results = cursor.fetchall()
        else:
            conn.commit()

    return results


def change_password(user_id, new_password):
    salt = secrets.token_hex(8)
    password_hash = pbkdf2_sha256.hash(new_password, rounds=800000, salt=str.encode(salt))[0:99]

    query = 'UPDATE [Users] SET [pwd_salt] = ?, [pwd_hash] = ? WHERE [Users].[ID] = ? '
    execute_query(query, [salt, password_hash, user_id], False)

    query2 = 'DELETE FROM [Sessions] WHERE [Sessions].[user_ID] = ?'
    execute_query(query2, [user_id], False)


def check_login(username, password):
    query = 'SELECT TOP (1) [ID], [pwd_hash], [pwd_salt] FROM Users WHERE [username] = ?'
    results = execute_query(query, [username.lower()], True)

    # if there are no results, no such user exists and the login is therefore unsuccessful
    if len(results) == 0:
        return False

    # retrieves the first and only result
    result = results[0]

    # checks if any columns are null
    if len(result) != 3 or result[1] is None or result[2] is None:
        return False

    # retrieves the salt from the database and strips any leading or trailing spaces
    salt = str.encode(result[2].strip())
    # calculates the hash
    calculated_hash = pbkdf2_sha256.hash(password, rounds=800000, salt=salt)[0:99]

    # checks if the calculated hash matches the one in the database
    if calculated_hash is not None and calculated_hash == result[1]:
        return True

    return False


def delete_post(post_id):
    query = 'UPDATE [Posts] SET [deleted] = 1 WHERE [id] = ?'
    execute_query(query, [post_id], False)


def create_session(user_id):
    query = 'INSERT INTO Sessions ([user_ID], [timestamp], [secret_token], [csrf_token])' \
            'VALUES (?, CURRENT_TIMESTAMP, ?, ?)'
    # generates base-64 tokens of length 192 bytes and 48 bytes respectively
    session_token = secrets.token_urlsafe(192)
    csrf_token = secrets.token_urlsafe(48)
    # executes the query without requesting results
    execute_query(query, [user_id, session_token, csrf_token], False)
    return [session_token, csrf_token]


def get_id_from_username(username):
    query = 'SELECT TOP (1) [ID] FROM Users WHERE [username] = ?'
    results = execute_query(query, [username], True)
    if len(results) == 0:
        return None
    # returns the first column of the result
    return results[0][0]


def user_is_subscribed(user_id, community_id):
    query = 'SELECT COUNT(*) FROM [Community_subscriptions] ' \
            'WHERE [Community_subscriptions].[community_id] = ? AND [Community_subscriptions].[user_id] = ?'
    results = execute_query(query, [community_id, user_id], True)

    if results is None or len(results) == 0 or results[0][0] == 0:
        return False
    else:
        return True


def get_community_id(community_name):
    query = 'SELECT [community_id] FROM Communities WHERE lower([name]) = lower(?)'
    results = execute_query(query, [community_name], True)
    if len(results) > 0:
        return results[0][0]
    else:
        return None


def get_community_posts(community_id, user_id, limit):
    query = 'SELECT TOP (?) [Posts].[id] ,[post_content] ,[author_id] ,[title] ,[community]' \
            ',[deleted], [timestamp]' \
            ',[Users].[username], DATEDIFF(SS, [timestamp], CURRENT_TIMESTAMP) AS [time_since], ' \
            '( ' \
            '    SELECT COUNT(*) FROM [Post_likes] WHERE [Post_likes].[post_id] = [Posts].[id] ' \
            '    AND [Post_likes].[user_id] = ? AND [Post_likes].[deleted] = 0 ' \
            ') ' \
            'FROM [TT_DB].[dbo].[Posts] INNER JOIN [dbo].[Users] ON ' \
            '[dbo].[Users].[ID] = [author_id] WHERE [community] = ? AND [deleted] = 0 ORDER BY [timestamp] desc'
    results = execute_query(query, [limit, user_id, community_id], True)
    out = []

    for result in results:
        row = dict()
        row['id'] = result[0]
        row['post-content'] = result[1]
        row['author-name'] = result[7]
        row['title'] = result[3]
        row['timestamp'] = result[8]
        if result[9] > 0:
            row['is-liked'] = True
        else:
            row['is-liked'] = False

        out.append(row)

    return out


def get_home_posts(user_id):
    query = 'SELECT [Posts].[id], [Posts].[post_content], [Posts].[author_id], [Posts].[title], [Posts].[community], ' \
            '[Communities].[name] AS [community_name], ' \
            '[Posts].[deleted], [Posts].[timestamp], [Users].[username], ' \
            'DATEDIFF(SS, [timestamp], CURRENT_TIMESTAMP) AS [time_since], ' \
            '   ( ' \
            '       SELECT COUNT(*) FROM [Post_likes] WHERE [Post_likes].[post_id] = [Posts].[id] ' \
            '       AND [Post_likes].[user_id] = ? AND [Post_likes].[deleted] = 0 ' \
            ') AS [is_liked], ' \
            'DATEDIFF(DD, [timestamp], CURRENT_TIMESTAMP) AS [days_ago], ' \
            '   ( ' \
            '       SELECT COUNT(*) FROM [Post_likes] WHERE [Post_likes].[post_id] = [Posts].[id] ' \
            '       AND [Post_likes].[deleted] = 0 ' \
            ') AS [total_likes], ' \
            '   ( ' \
            '       SELECT COUNT(*) FROM [Community_subscriptions] AS [CS] ' \
            '       WHERE [CS].[community_id] = [Communities].[community_id] ' \
            '       AND [CS].[user_id] = ? ' \
            ') AS [is_subscribed] ' \
            'FROM [Posts] ' \
            '   INNER JOIN [Users] ON [Users].[ID] = [author_id] AND [deleted] = 0 ' \
            '   INNER JOIN [Communities] ON [Communities].[community_id] = [Posts].[community] ' \
            '   WHERE [Posts].[deleted] = 0 ' \
            'ORDER BY [days_ago] asc, [is_subscribed] desc, [total_likes] desc '

    results = execute_query(query, [user_id, user_id], True)
    out = []

    for result in results:
        row = dict()
        row['id'] = result[0]
        row['post-content'] = result[1]
        row['author-name'] = result[8]
        row['title'] = result[3]
        row['timestamp'] = result[9]
        row['community'] = result[5]

        if result[10] > 0:
            row['is-liked'] = True
        else:
            row['is-liked'] = False

        if result[13] > 0:
            row['is-subscribed-to-community'] = True
        else:
            row['is-subscribed-to-community'] = False

        out.append(row)

    return out


def get_session_info(session_token, csrf_token):
    if session_token is None or csrf_token is None:
        return None

    query = 'SELECT TOP(1) [session_ID], [user_ID], [Users].[username], [timestamp], [secret_token], [csrf_token], ' \
            '[Users].[role] ' \
            'FROM [dbo].[Sessions] ' \
            'INNER JOIN [Users] ON [Users].[ID] = [Sessions].[user_ID] ' \
            'WHERE [Sessions].[secret_token] = ? AND [Sessions].[csrf_token] = ?'

    results = execute_query(query, [session_token, csrf_token], True)

    if results is None or len(results) == 0:
        return None
    else:
        return results


def post_is_liked(post_id, user_id):
    query = 'SELECT COUNT(*) FROM [Post_likes] ' \
            'WHERE [Post_likes].[post_id] = ? ' \
            'AND [Post_likes].[user_id] = ? ' \
            'AND [Post_likes].[deleted] = 0 '

    results = execute_query(query, [post_id, user_id], True)

    if results is None or len(results) == 0:
        return None
    else:
        if results[0][0] > 0:
            return True
        else:
            return False


def create_community_post(post_id, post_content, author_id, title, community):
    query = 'INSERT INTO [dbo].[Posts] ([id], [post_content], [author_id], [title], ' \
            '[community], [deleted], [timestamp]) ' \
            'VALUES (?, ?, ?, ?, ?, 0, CURRENT_TIMESTAMP)'

    execute_query(query, [post_id, post_content, author_id, title, community], False)


def get_post_content(post_id):
    query = 'SELECT TOP(1) [Posts].[author_id], [Users].[username], [Posts].[community], [Posts].[post_content], ' \
            'DATEDIFF(SS, [timestamp], CURRENT_TIMESTAMP) AS [time_since], ' \
            '[Posts].[title] FROM [Posts] INNER JOIN [Users] ' \
            'ON [Posts].[author_id] = [Users].[ID] WHERE [Posts].[id] = ? AND [Posts].[deleted] = 0'

    results = execute_query(query, [post_id], True)

    if results is None or len(results) == 0:
        return None

    return results[0]


def get_post_comments(post_id):
    query = 'SELECT [Comments].[ID], [Comments].[author_id], [Users].[username], [Users].[muted], ' \
            '[Comments].[comment_content], [Comments].[parent_comment], [Comments].[parent_post], ' \
            '[Comments].[timestamp], DATEDIFF(SS, [timestamp], CURRENT_TIMESTAMP) AS [time_since] ' \
            'FROM [Comments] INNER JOIN [Users] ON [Users].[ID] = [Comments].[author_id] ' \
            'WHERE [Comments].[parent_post] = ? ' \
            'ORDER BY [Comments].[timestamp] DESC'

    results = execute_query(query, [post_id], True)
    out = list()

    while True:
        if len(results) == 0:
            break

        for i in range(len(results)):
            c = results[i]
            comment_object = {
                'ID': c[0],
                'author-name': c[2],
                'muted': c[3],
                'content': c[4],
                'children': list()
            }

            if c[7] is not None:
                comment_object['timestamp'] = c[8]

            if c[5] is None:
                out.append(comment_object)
                results.pop(i)
                break
            else:
                if find_and_append_to_parent_comment(out, comment_object, c[5]):
                    results.pop(i)
                    break

    return out


def find_and_append_to_parent_comment(comment_tree, new_comment, desired_parent):
    for i in range(len(comment_tree)):
        if comment_tree[i]['ID'] == desired_parent:
            comment_tree[i]['children'].append(new_comment)
            return True

        if find_and_append_to_parent_comment(comment_tree[i]['children'], new_comment, desired_parent):
            return True

    return False


def create_account(username, password):
    query = 'INSERT INTO [Users] ([username], [ID], [pwd_salt], [pwd_hash], [role], [muted]) VALUES (?, ?, ?, ?, ?, ?)'

    salt = secrets.token_hex(8)
    calculated_hash = pbkdf2_sha256.hash(password, rounds=800000, salt=str.encode(salt))[0:99]

    execute_query(query, [username, random.randrange(1000, 999999999), salt, calculated_hash, 1, 0], False)


def check_comment_exists(comment_id):
    query = 'SELECT TOP(1) * FROM [Comments] WHERE [Comments].[ID] = ?'
    return len(execute_query(query, [comment_id], True)) > 0


def add_comment(comment_id, comment_content, author_id, parent_post, parent_comment):
    query = 'INSERT INTO [dbo].[Comments] ([ID], [comment_content], [author_id], [parent_post], [parent_comment], ' \
            '[timestamp]) VALUES (?, ?, ?, ?, ?, CURRENT_TIMESTAMP)'

    execute_query(query, [comment_id, comment_content, author_id, parent_post, parent_comment], False)


def get_all_communities():
    query = 'SELECT [name] FROM [Communities]'
    return execute_query(query, [], True)


def get_community_name(community_id):
    query = 'SELECT [Communities].[name] FROM [Communities] WHERE [Communities].[community_id] = ?'
    results = execute_query(query, [community_id], True)

    if results is None or len(results) == 0:
        return None
    else:
        return results[0][0]


def get_community_description(community_id):
    query = 'SELECT [Communities].[description] FROM [Communities] WHERE [Communities].[community_id] = ?'
    return execute_query(query, [community_id], True)


def like_post(user_id, post_id, remove_like):
    if remove_like:
        query = 'UPDATE [dbo].[Post_likes] SET [deleted] = 1 ' \
                'WHERE [Post_likes].[post_id] = ? AND [Post_likes].[user_id] = ?'
        try:
            execute_query(query, [post_id, user_id], False)
        except:
            return False
    else:
        query = 'INSERT INTO [Post_likes] ([user_id], [post_id], [deleted]) VALUES (?, ?, 0)'
        try:
            execute_query(query, [user_id, post_id], False)
        except:
            try:
                query = 'UPDATE [dbo].[Post_likes] SET [deleted] = 0 ' \
                        'WHERE [Post_likes].[post_id] = ? AND [Post_likes].[user_id] = ?'
                execute_query(query, [post_id, user_id], False)
            except:
                return False
    return True


def subscribe_to_community(user_id, community_id, remove_subscription):
    if remove_subscription:
        query = 'DELETE FROM [Community_subscriptions] ' \
                'WHERE [Community_subscriptions].[community_id] = ? AND [Community_subscriptions].[user_id] = ? '
        execute_query(query, [community_id, user_id], False)
    else:
        query = 'INSERT INTO [Community_subscriptions] ([community_id], [user_id]) VALUES (?, ?) '
        try:
            execute_query(query, [community_id, user_id], False)
        except:
            return False

    return True


def like_comment(user_id, comment_id, remove_like):
    if remove_like:
        query = 'UPDATE [dbo].[Comment_likes] SET [deleted] = 1 ' \
                'WHERE [Comment_likes].[comment_id] = ? AND [Comment_likes].[user_id] = ?'
        try:
            execute_query(query, [comment_id, user_id], False)
        except:
            return False
    else:
        query = 'INSERT INTO [Comment_likes] ([user_id], [post_id], [deleted]) VALUES (?, ?, 0)'
        try:
            execute_query(query, [user_id, comment_id], False)
        except:
            try:
                query = 'UPDATE [dbo].[Comment_likes] SET [deleted] = 0 ' \
                        'WHERE [Comment_likes].[comment_id] = ? AND [Comment_likes].[user_id] = ?'
                execute_query(query, [comment_id, user_id], False)
            except:
                return False
    return True
